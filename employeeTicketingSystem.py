from abc import ABC, abstractclassmethod

class Person(ABC):
	@abstractclassmethod
	def getFullName(self):
		pass
	def addRequest(self):
		pass
	def checkRequest(self):
		pass
	def addUser(self):
		pass

class Employee(Person):
	def __init__(self, fname, lname, email, department):
		super().__init__()
		self._firstName = fname
		self._lastName = lname
		self._email = email
		self._department = department

	#setter
	def set_firstName(self, fname):
		self._firstName = fname
	def set_lastName(self, lname):
		self._lastName = lname
	def set_email(self, email):
		self._email = email
	def set_department(self, department):
		self._department = department

	#getter
	def get_firstName(self):
		print(f'first name: {self._firstName}')
	def get_lastName(self):
		print(f'last name: {self._lastName}')
	def get_email(self):
		print(f'email: {self._email}')
	def get_department(self):
		print(f'department: {self._department}')

	def login(self):
		return f"{self._email} has logged in"
	def logout(self):
		return f"{self._email} has logged out"

	#inherited the Person class
	def getFullName(self):
		return f"{self._firstName} {self._lastName}"
	def addRequest(self):
		return "Request has been added"


class TeamLead(Person):
	def __init__(self, fname, lname, email, department):
		super().__init__()
		self._firstName = fname
		self._lastName = lname
		self._email = email
		self._department = department
		self._members = []

	#setter
	def set_firstName(self, fname):
		self._firstName = fname
	def set_lastName(self, lname):
		self._lastName = lname
	def set_email(self, email):
		self._email = email
	def set_department(self, department):
		self._department = department

	#getter
	def get_firstName(self):
		print(f'first name: {self._firstName}')
	def get_lastName(self):
		print(f'last name: {self._lastName}')
	def get_email(self):
		print(f'email: {self._email}')
	def get_department(self):
		print(f'department: {self._department}')

	def checkRequest(self):
		return "Checked request"
	def addUser(self):
		pass
	def login(self):
		return f"{self._email} has logged in"
	def logout(self):
		return f"{self._email} has logged out"
	def addMember(self, member):
		self._members.append(member)
		return "User has been added"
	def get_members(self):
		return self._members

	#inherited the Person class
	def getFullName(self):
		return f"{self._firstName} {self._lastName}"
	def addRequest(self):
		return "Request has been added"


class Admin(Person):
	def __init__(self, fname, lname, email, department):
		super().__init__()
		self._firstName = fname
		self._lastName = lname
		self._email = email
		self._department = department
		self.users = []

	#setter
	def set_firstName(self, fname):
		self._firstName = fname
	def set_lastName(self, lname):
		self._lastName = lname
	def set_email(self, email):
		self._email = email
	def set_department(self, department):
		self._department = department

	#getter
	def get_firstName(self):
		print(f'first name: {self._firstName}')
	def get_lastName(self):
		print(f'last name: {self._lastName}')
	def get_email(self):
		print(f'email: {self._email}')
	def get_department(self):
		print(f'department: {self._department}')

	def login(self):
		return f"{self._email} has logged in"
	def logout(self):
		return f"{self._email} has logged out"
	def addUser(self):
		return "User has been added"

	#inherited the Person class
	def getFullName(self):
		return f"{self._firstName} {self._lastName}"
	def addRequest(self):
		return "Request has been added"


class Request():
	def __init__(self, name, requester, dateReq):
		self._name = name
		self._requester = requester.getFullName()
		self._dateReq = dateReq
		self._status = "Open"

	#setter
	def set_name(self, name):
		self._name = name
	def set_requester(self, requester):
		self._requester = requester.getFullName()
	def set_dateReq(self, dateReq):
		self._dateReq = dateReq
	def set_status(self, status):
		self._status = status

	#getter
	def get_requester(self):
		print(f'{self._requester}')
	def get_status(self):
		return self._status

	def updateRequest(self, newReq):
		self._name = newReq
	def closeRequest(self):
		self._status = "Closed"
		return f"{self._name} has been closed"
	def cancelRequest(self):
		self._status = "cancelled"
		return f"{self._name} has been cancelled"



#Test cases:
employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")

assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

teamLead1.addMember(employee3)
teamLead1.addMember(employee4)
for indiv_emp in teamLead1.get_members():
    print(indiv_emp.getFullName())

assert admin1.addUser() == "User has been added"

req2.set_status("closed")
print(req2.closeRequest())